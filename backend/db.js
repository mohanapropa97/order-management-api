const mysql = require('mysql2');

const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '1234',
    database: 'shop'
});

db.connect((err) => {
    if (err) {
        console.error('Error connecting to MySQL: ' + err.stack);
        return;
    }
    console.log('Connected to MySQL');

    
    createTables();
});

function createTables() {
    createProductTable();
    createUserTable();
    createOrderTable();
    createPaymentTable();
}

function createProductTable() {
    const createProductTableQuery = `
        CREATE TABLE IF NOT EXISTS products (
            id INT AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(255) NOT NULL,
            price int NOT NULL,
            availability BOOLEAN NOT NULL
        )ENGINE=InnoDB;;
    `;

    db.query(createProductTableQuery, (err) => {
        if (err) {
            console.error('Error creating product table:', err.message);
        } else {
            console.log('Product table created successfully');
        }
    });
}

function createUserTable() {
    const createUserTableQuery = `
        CREATE TABLE IF NOT EXISTS users (
            id INT AUTO_INCREMENT PRIMARY KEY,
            username VARCHAR(255) NOT NULL,
            password VARCHAR(255) NOT NULL,
            role VARCHAR(50) NOT NULL
        )ENGINE=InnoDB;;
    `;

    db.query(createUserTableQuery, (err) => {
        if (err) {
            console.error('Error creating user table:', err.message);
        } else {
            console.log('User table created successfully');
        }
    });
}

function createOrderTable() {
    const createOrderTableQuery = `
        CREATE TABLE IF NOT EXISTS orders (
            id INT AUTO_INCREMENT PRIMARY KEY,
            user_id INT NOT NULL,
            product_id INT NOT NULL,
            quantity INT NOT NULL,
            INDEX (id), 
            FOREIGN KEY (user_id) REFERENCES users(id),
            FOREIGN KEY (product_id) REFERENCES products(id)
        )ENGINE=InnoDB;;
    
    `;

    db.query(createOrderTableQuery, (err) => {
        if (err) {
            console.error('Error creating order table:', err.message);
        } else {
            console.log('Order table created successfully');
        }
    });
}

function createPaymentTable() {
    const createPaymentTableQuery = `

    CREATE TABLE IF NOT EXISTS payments (
        id INT AUTO_INCREMENT PRIMARY KEY,
        order_id INT NOT NULL,
        payment_status VARCHAR(50) NOT NULL,
        amount DECIMAL(10, 2) NOT NULL,
        FOREIGN KEY (order_id) REFERENCES orders(id)
    )ENGINE=InnoDB;;
    
        
    `;

    db.query(createPaymentTableQuery, (err) => {
        if (err) {
            console.error('Error creating payment table:', err.message);
        } else {
            console.log('Payment table created successfully');
        }
    });
}

module.exports = db;
