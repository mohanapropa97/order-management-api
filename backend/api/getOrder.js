const express = require('express');
const router = express.Router();
const db = require('../db'); 

router.get('/getOrder/:orderId', (req, res) => {
    const orderId = req.params.orderId;

    const sql = 'SELECT * FROM orders WHERE order_id = ?';
    const values = [orderId];

    db.query(sql, values, (err, result) => {
        if (err) {
            console.error('Error retrieving order: ' + err.message);
            res.status(500).json({ error: 'Internal Server Error' });
            return;
        }

        if (result.length === 0) {
            res.status(404).json({ error: 'Order not found' });
            return;
        }

        const order = result[0];
        res.status(200).json(order);
    });
});

router.get('/getAllOrders', (req, res) => {
    const sql = 'SELECT * FROM orders';

    db.query(sql, (err, result) => {
        if (err) {
            console.error('Error retrieving orders: ' + err.message);
            res.status(500).json({ error: 'Internal Server Error' });
            return;
        }

        res.status(200).json(result);
    });
});

module.exports = router;
