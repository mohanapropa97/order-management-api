const express = require('express');
const router = express.Router();
const db = require('../db'); 

router.put('/updateOrder/:orderId', (req, res) => {
    const orderId = req.params.orderId;
    const { user_id, product_id, quantity, payment_info } = req.body;

    const sql = 'UPDATE orders SET user_id = ?, product_id = ?, quantity = ?, payment_info = ? WHERE order_id = ?';
    const values = [user_id, product_id, quantity, payment_info, orderId];

    db.query(sql, values, (err, result) => {
        if (err) {
            console.error('Error updating order: ' + err.message);
            res.status(500).json({ error: 'Internal Server Error' });
            return;
        }

        if (result.affectedRows === 0) {
            res.status(404).json({ error: 'Order not found' });
            return;
        }

        res.status(200).json({ message: 'Order updated successfully' });
    });
});

module.exports = router;
