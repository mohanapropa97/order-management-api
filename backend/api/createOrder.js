const express = require('express');
const router = express.Router();
const db = require('../db'); 

router.post('/createOrder', (req, res) =>  {
    const { user_id, product_id, quantity, payment_info } = req.body;


    const checkProductSql = 'SELECT * FROM products WHERE product_id = ?';
    const checkProductValues = [product_id];

    db.query(checkProductSql, checkProductValues, (productErr, productResult) => {
        if (productErr) {
            console.error('Error checking product: ' + productErr.message);
            res.status(500).json({ error: 'Internal Server Error' });
            return;
        }

        if (productResult.length === 0) {
           
            res.status(404).json({ error: 'Product not found' });
            return;
        }

        const createOrderSql = 'INSERT INTO orders (user_id, product_id, quantity, payment_info) VALUES (?, ?, ?, ?)';
        const createOrderValues = [user_id, product_id, quantity, payment_info];

        db.query(createOrderSql, createOrderValues, (orderErr, orderResult) => {
            if (orderErr) {
                console.error('Error creating order: ' + orderErr.message);
                res.status(500).json({ error: 'Internal Server Error' });
                return;
            }

            res.status(201).json({ message: 'Order created successfully', orderId: orderResult.insertId });
        });
    });
});

module.exports = router;
