const express = require('express');
const router = express.Router();
const db = require('../db');

router.delete('/deleteOrder/:orderId', (req, res) => {
    const orderId = req.params.orderId;

    const sql = 'DELETE FROM orders WHERE order_id = ?';
    const values = [orderId];

    db.query(sql, values, (err, result) => {
        if (err) {
            console.error('Error deleting order: ' + err.message);
            res.status(500).json({ error: 'Internal Server Error' });
            return;
        }

        if (result.affectedRows === 0) {
            res.status(404).json({ error: 'Order not found' });
            return;
        }

        res.status(200).json({ message: 'Order deleted successfully' });
    });
});

module.exports = router;
