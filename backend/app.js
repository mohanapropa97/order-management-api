const express = require('express');
const bodyParser = require('body-parser');
// const createOrderRoute = require('./api/createOrder');
// const updateOrderRoute = require('./api/updateOrder');
// const deleteOrderRoute = require('./api/deleteOrder');
// const getOrderRoute = require('./api/getOrder');


const productService = require('./microservice/product-service');
const userService = require('./microservice/user-service');
const orderService = require('./microservice/order-service');
const paymentService = require('./microservice/payment-service');
const app = express();
const port = 3000;

app.use(bodyParser.json());
// app.use(createOrderRoute);
// app.use(getOrderRoute);
// app.use(updateOrderRoute);
// app.use(deleteOrderRoute);
app.use(productService);
app.use(userService);
app.use(orderService);
app.use(paymentService);

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
