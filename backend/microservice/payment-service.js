const express = require('express');
const bodyParser = require('body-parser');
const db = require('../db');

const paymentService = express.Router();
paymentService.use(bodyParser.json());


paymentService.post('/createPayment', async (req, res) => {
    const { order_id, payment_status } = req.body;

    const checkOrderExistenceQuery = 'SELECT * FROM orders WHERE id = ?';

    try {
        const orderResults = await db.promise().query(checkOrderExistenceQuery, [order_id]);

        if (orderResults[0].length === 0) {
            return res.status(404).json({ error: 'Order not found' });
        }

        const order = orderResults[0][0];
        const product_id = order.product_id;
        const quantity = order.quantity;
        const getProductPriceQuery = 'SELECT price FROM products WHERE id = ?';
        const productPriceResults = await db.promise().query(getProductPriceQuery, [product_id]);
        const productPrice = productPriceResults[0][0].price;
        const amount = productPrice * quantity;
        const insertPaymentQuery = 'INSERT INTO payments (order_id, payment_status, amount) VALUES (?, ?, ?)';
        const paymentResults = await db.promise().query(insertPaymentQuery, [order_id, payment_status, amount]);

        const paymentId = paymentResults[0].insertId;
        res.json({ message: 'Payment created successfully', paymentId });
    } catch (error) {
        console.error('Error creating payment:', error.message);
        res.status(500).json({ error: 'Error creating payment' });
    }
});


paymentService.get('/getPayment', async (req, res) => {
    const getAllPaymentsQuery = 'SELECT * FROM payments';

    try {
        const paymentsResults = await db.promise().query(getAllPaymentsQuery);
        res.json({ payments: paymentsResults[0] });
    } catch (error) {
        console.error('Error fetching payments:', error.message);
        res.status(500).json({ error: 'Error fetching payments' });
    }
});


paymentService.get('/getPayment/:id', async (req, res) => {
    const paymentId = req.params.id;
    const getPaymentByIdQuery = 'SELECT * FROM payments WHERE id = ?';

    try {
        const paymentResults = await db.promise().query(getPaymentByIdQuery, [paymentId]);

        if (paymentResults[0].length === 0) {
          
            return res.status(404).json({ error: 'Payment not found' });
        }

        res.json({ payment: paymentResults[0][0] });
    } catch (error) {
        console.error('Error fetching payment:', error.message);
        res.status(500).json({ error: 'Error fetching payment' });
    }
});


paymentService.put('/updatePayment/:id', async (req, res) => {
    const paymentId = req.params.id;
    const { order_id, payment_status, amount } = req.body;

    const checkPaymentExistenceQuery = 'SELECT * FROM payments WHERE id = ?';

    try {
        const paymentResults = await db.promise().query(checkPaymentExistenceQuery, [paymentId]);

        if (paymentResults[0].length === 0) {
            return res.status(404).json({ error: 'Payment not found' });
        }

        const checkOrderExistenceQuery = 'SELECT * FROM orders WHERE id = ?';
        const orderResults = await db.promise().query(checkOrderExistenceQuery, [order_id]);

        if (orderResults[0].length === 0) {
            return res.status(404).json({ error: 'Order not found' });
        }

        const updatePaymentQuery = 'UPDATE payments SET order_id = ?, payment_status = ?, amount = ? WHERE id = ?';
        await db.promise().query(updatePaymentQuery, [order_id, payment_status, amount, paymentId]);

        res.json({ message: 'Payment updated successfully' });
    } catch (error) {
        console.error('Error updating payment:', error.message);
        res.status(500).json({ error: 'Error updating payment' });
    }
});



paymentService.delete('/deletePayment/:id', async (req, res) => {
    const paymentId = req.params.id;
    const deletePaymentQuery = 'DELETE FROM payments WHERE id = ?';

    try {
        await db.promise().query(deletePaymentQuery, [paymentId]);
        res.json({ message: 'Payment deleted successfully' });
    } catch (error) {
        console.error('Error deleting payment:', error.message);
        res.status(500).json({ error: 'Error deleting payment' });
    }
});

module.exports = paymentService;
