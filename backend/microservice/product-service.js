const express = require('express');
const bodyParser = require('body-parser');
const db = require('../db'); 

const productService = express.Router();
productService.use(bodyParser.json());

// API to create a new product
productService.post('/createProducts', (req, res) => {
    const { name, price, availability } = req.body;
    const insertProductQuery = `
        INSERT INTO products (name, price, availability)
        VALUES (?, ?, ?);
    `;

    db.query(insertProductQuery, [name, price, availability], (err, results) => {
        if (err) {
            console.error('Error creating product:', err.message);
            res.status(500).json({ error: 'Error creating product' });
        } else {
            const productId = results.insertId;
            res.json({ message: 'Product created successfully', productId });
        }
    });
});

// API to get all products
productService.get('/getProducts', (req, res) => {
    const getAllProductsQuery = 'SELECT * FROM products';

    db.query(getAllProductsQuery, (err, results) => {
        if (err) {
            console.error('Error fetching products:', err.message);
            res.status(500).json({ error: 'Error fetching products' });
        } else {
            res.json({ products: results });
        }
    });
});

// API to get a specific product by ID
productService.get('/getProducts/:id', (req, res) => {
    const productId = req.params.id;
    const getProductByIdQuery = 'SELECT * FROM products WHERE id = ?';

    db.query(getProductByIdQuery, [productId], (err, results) => {
        if (err) {
            console.error('Error fetching product:', err.message);
            res.status(500).json({ error: 'Error fetching product' });
        } else {
            if (results.length === 0) {
                res.status(404).json({ error: 'Product not found' });
            } else {
                res.json({ product: results[0] });
            }
        }
    });
});

// API to update a product by ID
productService.put('/updateProducts/:id', (req, res) => {
    const productId = req.params.id;
    const { name, price, availability } = req.body;
    const updateProductQuery = `
        UPDATE products
        SET name = ?, price = ?, availability = ?
        WHERE id = ?;
    `;

    db.query(
        updateProductQuery,
        [name, price, availability, productId],
        (err, results) => {
            if (err) {
                console.error('Error updating product:', err.message);
                res.status(500).json({ error: 'Error updating product' });
            } else {
                if (results.affectedRows === 0) {
                    res.status(404).json({ error: 'Product not found' });
                } else {
                    res.json({ message: 'Product updated successfully' });
                }
            }
        }
    );
});

// API to delete a product by ID
productService.delete('/deleteProducts/:id', (req, res) => {
    const productId = req.params.id;
    const deleteProductQuery = 'DELETE FROM products WHERE id = ?';

    db.query(deleteProductQuery, [productId], (err, results) => {
        if (err) {
            console.error('Error deleting product:', err.message);
            res.status(500).json({ error: 'Error deleting product' });
        } else {
            if (results.affectedRows === 0) {
                res.status(404).json({ error: 'Product not found' });
            } else {
                res.json({ message: 'Product deleted successfully' });
            }
        }
    });
});

module.exports = productService;
