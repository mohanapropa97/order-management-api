const express = require('express');
const bodyParser = require('body-parser');
const db = require('../db');

const userService = express.Router();
userService.use(bodyParser.json());


userService.post('/createUser', (req, res) => {
    const { username, password, role } = req.body;
    const insertUserQuery = `
        INSERT INTO users (username, password, role)
        VALUES (?, ?, ?);
    `;

    db.query(insertUserQuery, [username, password, role], (err, results) => {
        if (err) {
            console.error('Error creating user:', err.message);
            res.status(500).json({ error: 'Error creating user' });
        } else {
            const userId = results.insertId;
            res.json({ message: 'User created successfully', userId });
        }
    });
});


userService.get('/getUser', (req, res) => {
    const getAllUsersQuery = 'SELECT * FROM users';

    db.query(getAllUsersQuery, (err, results) => {
        if (err) {
            console.error('Error fetching users:', err.message);
            res.status(500).json({ error: 'Error fetching users' });
        } else {
            res.json({ users: results });
        }
    });
});


userService.get('/getUser/:id', (req, res) => {
    const userId = req.params.id;
    const getUserByIdQuery = 'SELECT * FROM users WHERE id = ?';

    db.query(getUserByIdQuery, [userId], (err, results) => {
        if (err) {
            console.error('Error fetching user:', err.message);
            res.status(500).json({ error: 'Error fetching user' });
        } else {
            if (results.length === 0) {
                res.status(404).json({ error: 'User not found' });
            } else {
                res.json({ user: results[0] });
            }
        }
    });
});


userService.put('/updateUser/:id', (req, res) => {
    const userId = req.params.id;
    const { username, password, role } = req.body;
    const updateUserQuery = `
        UPDATE users
        SET username = ?, password = ?, role = ?
        WHERE id = ?;
    `;

    db.query(updateUserQuery, [username, password, role, userId], (err, results) => {
        if (err) {
            console.error('Error updating user:', err.message);
            res.status(500).json({ error: 'Error updating user' });
        } else {
            if (results.affectedRows === 0) {
                res.status(404).json({ error: 'User not found' });
            } else {
                res.json({ message: 'User updated successfully' });
            }
        }
    });
});

// API to delete a user by ID
userService.delete('/deleteUser/:id', (req, res) => {
    const userId = req.params.id;
    const deleteUserQuery = 'DELETE FROM users WHERE id = ?';

    db.query(deleteUserQuery, [userId], (err, results) => {
        if (err) {
            console.error('Error deleting user:', err.message);
            res.status(500).json({ error: 'Error deleting user' });
        } else {
            if (results.affectedRows === 0) {
                res.status(404).json({ error: 'User not found' });
            } else {
                res.json({ message: 'User deleted successfully' });
            }
        }
    });
});

module.exports = userService;
