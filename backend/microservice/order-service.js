// order-service.js
const express = require('express');
const bodyParser = require('body-parser');
const db = require('../db');

const orderService = express.Router();
orderService.use(bodyParser.json());


orderService.post('/createOrder', async (req, res) => {
    const { user_id, product_id, quantity } = req.body;

    const checkProductAvailabilityQuery = 'SELECT * FROM products WHERE id = ? AND availability = true';

    try {
        const productResults = await db.promise().query(checkProductAvailabilityQuery, [product_id]);

        if (productResults[0].length === 0) {
            return res.status(404).json({ error: 'Product not found or not available' });
        }

        const insertOrderQuery = 'INSERT INTO orders (user_id, product_id, quantity) VALUES (?, ?, ?)';
        const orderResults = await db.promise().query(insertOrderQuery, [user_id, product_id, quantity]);

        const orderId = orderResults[0].insertId;
        res.json({ message: 'Order created successfully', orderId });
    } catch (error) {
        console.error('Error creating order:', error.message);
        res.status(500).json({ error: 'Error creating order' });
    }
});


orderService.get('/getOrder', (req, res) => {
    const getAllOrdersQuery = 'SELECT * FROM orders';

    db.query(getAllOrdersQuery, (err, results) => {
        if (err) {
            console.error('Error fetching orders:', err.message);
            res.status(500).json({ error: 'Error fetching orders' });
        } else {
            res.json({ orders: results });
        }
    });
});

orderService.get('/getOrder/:id', (req, res) => {
    const orderId = req.params.id;
    const getOrderByIdQuery = 'SELECT * FROM orders WHERE id = ?';

    db.query(getOrderByIdQuery, [orderId], (err, results) => {
        if (err) {
            console.error('Error fetching order:', err.message);
            res.status(500).json({ error: 'Error fetching order' });
        } else {
            if (results.length === 0) {
                res.status(404).json({ error: 'Order not found' });
            } else {
                res.json({ order: results[0] });
            }
        }
    });
});


orderService.put('/updateOrder/:id', (req, res) => {
    const orderId = req.params.id;
    const { user_id, product_id, quantity } = req.body;
    const updateOrderQuery = `
        UPDATE orders
        SET user_id = ?, product_id = ?, quantity = ?
        WHERE id = ?;
    `;

    db.query(updateOrderQuery, [user_id, product_id, quantity, orderId], (err, results) => {
        if (err) {
            console.error('Error updating order:', err.message);
            res.status(500).json({ error: 'Error updating order' });
        } else {
            if (results.affectedRows === 0) {
                res.status(404).json({ error: 'Order not found' });
            } else {
                res.json({ message: 'Order updated successfully' });
            }
        }
    });
});


orderService.delete('/deleteOrder/:id', (req, res) => {
    const orderId = req.params.id;
    const deleteOrderQuery = 'DELETE FROM orders WHERE id = ?';

    db.query(deleteOrderQuery, [orderId], (err, results) => {
        if (err) {
            console.error('Error deleting order:', err.message);
            res.status(500).json({ error: 'Error deleting order' });
        } else {
            if (results.affectedRows === 0) {
                res.status(404).json({ error: 'Order not found' });
            } else {
                res.json({ message: 'Order deleted successfully' });
            }
        }
    });
});

module.exports = orderService;
