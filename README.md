# Online Retail System

## Overview

This system contains the microservices architecture for an online retail system, designed to manage product, user accounts, orders, and payment processing.

## Microservices Components

1. **Product Service:**
   - **Database:** MySQL.
   - **Communication:** RESTful APIs.
   - **Transactions:** This services contains product name, price and its availability. The product table is linked up with order table.

2. **User Service:**
   - **Database:** MySQL
   - **Communication:** RESTful APIs.
   - **Transactions:** This services contains user name, password and user role. User id is linked up with order tale to make it clear that who is creating the order.

3. **Order Service:**
   - **Database:** MySQL
   - **Communication:** Rest APIs.
   - **Transactions:** This services contains user id, product id and the product quantity. So while creating an order it will track who created the order and the product purchasing details.

4. **Payment Processing Service:**
   - **Database:** MySQL
   - **Communication:** REST APIs
   - **Transactions:** This service contains order id, payment status and the total amount. It is linked up with order id.

## Communication and Data Consistency

1. **RESTful APIs:**
   - Synchronous communication for immediate responses.
   - Proper error handling for APIs.

   # Coding Part

The code contains four microservices: `product-service`, `user-service`, `order-service`, and `payment-service`.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Usage](#usage)
- [API Endpoints](#api-endpoints)

## Prerequisites

Before you begin, ensure you have the following installed:

- Node.js
- npm (Node Package Manager)
- MySQL Database
- Visual Studio Code (VS Code)
- Postman (API testing tool)

## Installation

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/mohanapropa97/order-management-api.git
    ```

2. Navigate to backend directory and install dependencies:

    ```bash
    cd backend
    npm install
    ```

3. Set up the database:

    - Create a shop schema in MySQL database.

4. Start each microservice:

    ```bash
    cd backend
    node app.js
    ```

## Usage

- Access the APIs through the mentioned endpoints below (see [API Endpoints](#api-endpoints)).
- Test the functionality using tools like Postman.

## API Endpoints

- **Product Service:**
  - POST `/createProducts`: Create a new product.
  - GET `/getProducts`: Get all products.
  - GET `/getProducts/:id`: Get a specific product by ID.
  - PUT `/updateProducts/:id`: Update a product by ID.
  - DELETE `/deleteProducts/:id`: Delete a product by ID.

- **User Service:**
  - POST `/createUser`: Create a new user.
  - GET `/getUser`: Get all users.
  - GET `/getUser/:id`: Get a specific user by ID.
  - PUT `/updateUser/:id`: Update a user by ID.
  - DELETE `/deleteUser/:id`: Delete a user by ID.

- **Order Service:**
  - POST `/createOrder`: Create a new order.
  - GET `/getOrder`: Get all orders.
  - GET `/getOrder/:id`: Get a specific order by ID.
  - PUT `/updateOrder/:id`: Update an order by ID.
  - DELETE `/deleteOrder/:id`: Delete an order by

  - **Payment Service:**
  - POST `/createPayment`: Create a new payment.
  - GET `/getPayment`: Get all payment details.
  - GET `/getPayment/:id`: Get a specific payment details by ID.
  - PUT `/updatePayment/:id`: Update payment by ID.
  - DELETE `/deletePayment/:id`: Delete apyment by ID.

  # Optimizing and Scaling the Application

This document outlines the strategies for scaling and optimizing a high-traffic web application built using Node.js and MySQL database.

## Database Choice

I have chosen MySQL as the database for the following reasons:

- **Reliability:** MySQL is a well-established relational database management system known for its reliability and ACID compliance.
- **Performance:** It provides excellent performance for read-heavy operations, which is common in web applications.
- **Scalability:** MySQL can scale horizontally with techniques such clustering.

## Node.js Libraries and Tools

- **Express:** A fast, unopinionated, minimalist web framework for Node.js.
- **mysql2:** A Node.js-based MySQL library that provides fast and efficient MySQL connections.


